create role framework login password 'framework';
create database framework;
alter database framework owner to framework;

create sequence s_dept;

create table Dept (
    idDept varchar(50) primary key not null,
    nom varchar(50)
);

insert into Dept(idDept,nom) values ('D'||nextval('s_dept'),'Finance'),
    ('D'||nextval('s_dept'),'Science'),
    ('D'||nextval('s_dept'),'Politique'); 




